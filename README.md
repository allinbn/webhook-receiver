### Webhook Receiver

This is currently setup to receive all webhooks for the allinbn group.

* [Webhook Configuration Page](https://gitlab.com/groups/allinbn/-/hooks/13414412/edit)
* [Project Page](https://gitlab.com/allinbn/webhook-receiver)
